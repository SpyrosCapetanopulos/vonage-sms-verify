const Nexmo = require('nexmo')
const app = require('express')()
var cors = require('cors')
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(cors())

require('dotenv').config();
const nexmoclient = new Nexmo({
    apiKey: process.env.API_KEY,
    apiSecret: process.env.API_SECRET
})

const sendSMS = (request, response) => {
    console.log(request.body);


    let text = request.body.message;
    let toNumber = request.body.number;

    nexmoclient.message.sendSms(process.env.FROM_NUMBER, toNumber, text, {
        type: "unicode"
    }, (err, responseData) => {
        if (err) {
            console.log(err);
        } else {
            if (responseData.messages[0]['status'] === "0") {
                console.log("Message sent successfully.");
                response.json("Message sent successfully.");
            } else {
                console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
                response.json(`Message failed with error: ${responseData.messages[0]['error-text']}`);
            }
        }
    })

}

const verifyRequest = (request, response) => {
    let toNumber = request.body.number;

    nexmoclient.verify.request({
        number: toNumber,
        brand: process.env.BRAND_NAME
    }, (err, result) => {
        if (err) {
            response.json(err);
            console.error(err);
        } else {
            const verifyRequestId = result.request_id
            console.log('request_id', verifyRequestId)
            response.json({
                request_id: verifyRequestId
            });
        }
    })
}

const verifyCheck = (request, response) => {

    let requestId = request.body.request_id;
    let codeVerify = request.body.code;

    nexmoclient.verify.check({
        request_id: requestId,
        code: codeVerify
    }, (err, result) => {
        if (err) {
            console.error(err);
            response.json(err);
        } else {
            console.log(result);
            response.json(result);
        }
    })
}

app
    .post('/api/sendsms', sendSMS)
    .post('/api/verify_request', verifyRequest)
    .post('/api/verify_check', verifyCheck)

app.listen(3000)