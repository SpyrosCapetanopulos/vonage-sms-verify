// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import AxiosPlugin from 'vue-axios-cors';

Vue.use(AxiosPlugin)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    phone_number: null
  },
  router,
  components: {
    App
  },
  template: '<App/>'
})
